class UserController {
  constructor() {}
  public getAllUsers(): void {}
  public getUser(): void {}
  public createUser(): void {}
  public updateUser(): void {}
  public deleteUser(): void {}
}

export default UserController;
