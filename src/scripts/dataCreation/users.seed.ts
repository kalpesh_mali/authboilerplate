import mongoose from 'mongoose';
import { faker } from '@faker-js/faker';
import { Command } from 'commander';
import userModel from '../../api/v1/models/schema/user.model';
import User from "../../api/v1/interfaces/user.interface";
import MongoDBConnection from '../../api/v1/models/connect';
import { MongoError } from 'mongodb';
import 'dotenv/config';



const User = userModel;
// Connect to your MongoDB database
const mongoDB = new MongoDBConnection(
  process.env.MONGO_USER,
  process.env.MONGO_PASSWORD,
  process.env.MONGO_PATH,
  {}
);


mongoDB.connectToSeed();

const program = new Command();

program.version('1.0.0').description('User Seeder');

// Function to generate a random date within a range
function getRandomDate(startDate: Date, endDate: Date): Date {
  const start = startDate.getTime();
  const end = endDate.getTime();
  const randomTime = start + Math.random() * (end - start);
  return new Date(randomTime);
}

// Function to generate fake user data
function generateFakeUsers(numUsers: number): User[] {
  const users: User[] = [];

  for (let i = 0; i < numUsers; i++) {
    const firstName = faker.person.firstName();
    const lastName = faker.person.lastName();
    const email = faker.internet.email();
    const password = faker.internet.password(10); // Adjust the password length as needed

    // Random date within a range for createdAt and updatedAt fields
    const createdAt = getRandomDate(new Date(2020, 0, 1), new Date());
    const updatedAt = getRandomDate(createdAt, new Date());

    const user = new User({
      firstName,
      lastName,
      email,
      password,
      createdAt,
      updatedAt,
    });

    users.push(user);
  }

  return users;
}

// Function to insert fake users in chunks


// Function to insert fake users in chunks with retry on duplicate key error
async function insertFakeUsers(
  users: User[],
  chunkSize: number
): Promise<void> {
  const retryAttempts = 3; // Number of retry attempts on duplicate key error
  const retryDelayMs = 1000; // Delay between retry attempts in milliseconds
  const totalUsers = users.length;
  let usersInserted = 0;

  // Retry loop
  for (let attempt = 1; attempt <= retryAttempts; attempt++) {
    try {
      while (users.length > 0) {
        const chunk = users.splice(0, chunkSize);
        await User.insertMany(chunk);
        usersInserted += chunk.length;

        console.log(`Inserted ${usersInserted} out of ${totalUsers} users...`);
      }

      console.log(`${usersInserted} users inserted successfully!`);
      return; // Exit the function if all users are inserted successfully
    } catch (error) {
      // Check if the error is a duplicate key error (code 11000)
      if (error instanceof MongoError && error.code === 11000) {
        console.warn(`Duplicate key error on attempt ${attempt}. Retrying...`);
        await new Promise((resolve) => setTimeout(resolve, retryDelayMs));
      } else {
        // If it's not a duplicate key error, throw the error
        console.error('Error inserting users:', error);
        throw error;
      }
    }
  }

  console.error(`Failed to insert all users after ${retryAttempts} attempts.`);
}


// Function to delete all users
async function deleteAllUsers(): Promise<void> {
  try {
    const deleteResult = await User.deleteMany({});
    console.log(`Deleted ${deleteResult.deletedCount} users`);
  } catch (error) {
    console.error('Error deleting users:', error);
  }
}

program
  .command('insert <numUsers>')
  .alias('i')
  .description('Insert fake users into the database')
  .action(async (numUsers: string) => {
    const num = parseInt(numUsers, 10);
    if (isNaN(num) || num <= 0) {
      console.error('Invalid number of users');
      return;
    }

    const users = generateFakeUsers(num);
    const chunkSize = 100; // Adjust the chunk size as needed

    console.log(`Inserting ${num} fake users into the database...`);
    await insertFakeUsers(users, chunkSize);
    mongoose.disconnect();
  });

program
  .command('delete')
  .alias('d')
  .description('Delete all users from the database')
  .action(async () => {
    console.log('Deleting all users from the database...');
    await deleteAllUsers();
    mongoose.disconnect();
  });

program.parse(process.argv);
